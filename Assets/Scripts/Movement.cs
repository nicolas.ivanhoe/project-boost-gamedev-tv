using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{

    Rigidbody rb;

    [SerializeField] float thrustPower = 20f;
    [SerializeField] float rotationAngle = 10f;
    [SerializeField] float maxTorque = 2f;

    void Awake() 
    {
        rb = GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void FixedUpdate()
    {
        ProcessThrust();
    }

    // Update is called once per frame
    void Update()
    {
        ProcessRotation();
    }

    private void ProcessRotation()
    {
        if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.A))
        {
            return;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            ApplyRotation(rotationAngle);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            ApplyRotation(-rotationAngle);
        }
    }

    private void ApplyRotation(float rotationThisFrame)
    {
        rb.freezeRotation = true; // freezing rotation so we can manually rotate
        transform.Rotate(Vector3.back * rotationThisFrame * Time.deltaTime);
        rb.freezeRotation = false; // unfreezing rotation so physics can take over
    }

    void ProcessThrust()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            rb.AddRelativeForce(Vector3.up * thrustPower * Time.deltaTime);
        }
    }

    /*  
        I played a bit trying to implement the mechanic using only physics with Torque.
        However it gets quite complex to play then.
        Leaving here the code for future reference.
    */
    void ProcessTorque()
    {
        if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.A))
        {
            return;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            rb.AddRelativeTorque(Vector3.back * rotationAngle * Time.deltaTime, ForceMode.Force);
            rb.angularVelocity = new Vector3(0, 0, Mathf.Clamp(rb.angularVelocity.z, -maxTorque, maxTorque));
        }
        else if (Input.GetKey(KeyCode.A))
        {
            rb.AddRelativeTorque(Vector3.forward * rotationAngle * Time.deltaTime, ForceMode.Force);
            rb.angularVelocity = new Vector3(0, 0, Mathf.Clamp(rb.angularVelocity.z, -maxTorque, maxTorque));
        }
    }
}
